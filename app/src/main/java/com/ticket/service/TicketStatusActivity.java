package com.ticket.service;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class TicketStatusActivity extends AppCompatActivity {

    private TextView txtCurrentServing;
    private TextView txtUserTicket;
    private TextView txtAverageTime;
    private String userTicketNumber;
    private String keyFirebaseKey;
    private String service;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_status);
        txtCurrentServing = findViewById(R.id.txtCurrentServing);
        txtUserTicket = findViewById(R.id.txtUserTicket);
        txtAverageTime = findViewById(R.id.txtAverageTime);
        userTicketNumber = Utils.getKeyTicketNumber(this);
        keyFirebaseKey = Utils.getTicketKey(this);
        txtUserTicket.setText("Your ticket number: " + userTicketNumber + "");
        service = Utils.getService(this);

        getCurrentServingTicket();

        setMonitorTicketCompleted();

    }

    private void setMonitorTicketCompleted() {

        String dateToday = Utils.getTodayDate();
        DatabaseReference ref = database.getReference("tickets/" + dateToday + "/" + service + "/" + keyFirebaseKey + "/isCompleted");
        Log.e("ref Path", ref.getPath().toString());
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {
                    String isCompleted = dataSnapshot.getValue().toString();
                    Log.e("isCompleted", isCompleted);
                    if (isCompleted.equals("true")) {

                        // Show Thank you message
                        Runnable mYesRunnable = new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(TicketStatusActivity.this, CreateNewTaskActivity.class));
                                finish();
                            }
                        };

                        Runnable mNoRunnable = new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        };


                        Utils.showInfoAlert(TicketStatusActivity.this, "Ticket completed!",
                                "Thank you for your patience.\nDo you want to create an other service?",
                                "No, Thanks", "Yes", mNoRunnable, mYesRunnable);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        ref.addValueEventListener(valueEventListener);
    }


    private void getCurrentServingTicket() {

        String dateToday = Utils.getTodayDate();
        DatabaseReference ref = database.getReference("tickets/" + dateToday + "/" + service + "");
        Query uidRef = ref.orderByChild("isCompleted").equalTo(false).limitToFirst(1);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    Log.e("onQueueChanged", dataSnapshot.getValue().toString());

                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        String ticketNumberCurrentlyServing = data.child("ticketNumber").getValue().toString();

                        String notificationTitle = "Alert! Queue has moved";
                        String notificationMessage = "";

                        if (ticketNumberCurrentlyServing.equals(Utils.getKeyTicketNumber(TicketStatusActivity.this))) {
                            txtCurrentServing.setText("Its your turn!\nTicket Number\n----\n" + ticketNumberCurrentlyServing + "\n---");
                            txtCurrentServing.setTextColor(Color.parseColor("#68AA13"));
                            notificationMessage = "Its your turn now! ticket Number: " + ticketNumberCurrentlyServing;
                        } else {
                            txtCurrentServing.setText("Currently Serving\nTicket Number\n----\n" + ticketNumberCurrentlyServing + "\n---");
                            txtCurrentServing.setTextColor(Color.parseColor("#3C3F41"));
                            notificationMessage = "Current ticket number: " + ticketNumberCurrentlyServing + "";
                        }

                        createNotification(notificationTitle, notificationMessage);

                        break;
                    }

                    // Get average time
                    getAverageTimePerTask();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }

    private void getAverageTimePerTask() {

        DatabaseReference ref = database.getReference("AverageTimePerTask");
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    String minutes = dataSnapshot.getValue().toString();
                    // Log.e("onQueueChanged", dataSnapshot.getValue().toString());
                    txtAverageTime.setText("Note: Average time taken for serving a individual token is " + minutes + " minute(s) approx. Please be patient and thanks for your cooperation");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        ref.addValueEventListener(valueEventListener);
    }

    private static final String EXTRA_STARTED_FROM_NOTIFICATION = TicketStatusActivity.class.getName() +
            ".started_from_notification";

    /**
     * The name of the channel for notifications.
     */
    private static final String CHANNEL_ID = "channel_01";

    /**
     * Returns the {@link NotificationCompat} used
     */

    private void createNotification(String title, String message) {
        Intent intent = new Intent(this, TicketStatusActivity.class);

        // CharSequence text = Utils.getLocationText(mLocation);

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .addAction(R.mipmap.ic_launcher, getString(R.string.launch_activity),
                        activityPendingIntent)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(message)
                .setWhen(System.currentTimeMillis());

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }

}
