package com.ticket.service;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class SplashScreenActivity extends AppCompatActivity {

    private TextView txtAppName;

    private String appNameSelectedFirstTime;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    final FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        txtAppName = findViewById(R.id.txtAppName);

        appNameSelectedFirstTime = Utils.getAppType(this);

        Log.e("appNameSelected", appNameSelectedFirstTime + "");

        if (appNameSelectedFirstTime.equals("CLIENT_APP")) {
            txtAppName.setText("Client App");
        } else if (appNameSelectedFirstTime.equals("ADMIN_APP")) {
            txtAppName.setText("Admin App");
        } else {
            txtAppName.setVisibility(View.GONE);
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (appNameSelectedFirstTime.equals("CLIENT_APP")) {

                    String key = Utils.getTicketKey(SplashScreenActivity.this);
                    String service = Utils.getService(SplashScreenActivity.this);
                    String dateToday = Utils.getTodayDate();

                    DatabaseReference ref = database.getReference("tickets/" + dateToday + "/" + service + "/" + key + "/isCompleted");
                    Log.e("ref Path", ref.getPath().toString());
                    ValueEventListener valueEventListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            if (dataSnapshot.getValue() != null) {
                                String isCompleted = dataSnapshot.getValue().toString();
                                Log.e("isCompleted", isCompleted);
                                if (isCompleted.equals("false")) {
                                    // Show status Activity
                                    startActivity(new Intent(SplashScreenActivity.this, TicketStatusActivity.class));
                                    finish();

                                } else {
                                    // Show Main Activity
                                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                                    finish();
                                }
                            } else {
                                // Show main Main Activity
                                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Log.e("onCancelled", databaseError.getMessage()); // Log errors
                        }
                    };
                    ref.addListenerForSingleValueEvent(valueEventListener);

                } else if (appNameSelectedFirstTime.equals("ADMIN_APP")) {
                    startActivity(new Intent(SplashScreenActivity.this, AdminActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashScreenActivity.this, AppSelectionActivity.class));
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

}

