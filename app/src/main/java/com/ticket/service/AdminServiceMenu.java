package com.ticket.service;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class AdminServiceMenu extends AppCompatActivity implements View.OnClickListener {

    private Button btnLetters, btnNewStudentReg, btnModuleReg, btnNotifications, btnTimeTable, btnFinanceCashier, btnCertificates, btnResults, btnLogout;

    private ArrayList<Button> btnArray = new ArrayList<>();
    final FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_service_menu);

        btnLetters = findViewById(R.id.btnLetters);
        btnLetters.setOnClickListener(this);
        btnNewStudentReg = findViewById(R.id.btnNewStudentReg);
        btnNewStudentReg.setOnClickListener(this);
        btnModuleReg = findViewById(R.id.btnModuleReg);
        btnModuleReg.setOnClickListener(this);
        btnNotifications = findViewById(R.id.btnNotifications);
        btnNotifications.setOnClickListener(this);
        btnTimeTable = findViewById(R.id.btnTimeTable);
        btnTimeTable.setOnClickListener(this);
        btnFinanceCashier = findViewById(R.id.btnFinanceCashier);
        btnFinanceCashier.setOnClickListener(this);
        btnCertificates = findViewById(R.id.btnCertificates);
        btnCertificates.setOnClickListener(this);
        btnResults = findViewById(R.id.btnResults);
        btnResults.setOnClickListener(this);

        btnArray.add(btnLetters);
        btnArray.add(btnNewStudentReg);
        btnArray.add(btnModuleReg);
        btnArray.add(btnNotifications);
        btnArray.add(btnTimeTable);
        btnArray.add(btnFinanceCashier);
        btnArray.add(btnCertificates);
        btnArray.add(btnResults);

        btnLogout = findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(AdminServiceMenu.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(new Intent(AdminServiceMenu.this, AdminActivity.class));
            }
        });

        getAllCountForAllServices();
    }

    @Override
    public void onClick(View v) {

        String requestName = v.getTag().toString().replace(" ", "_");
        Log.e("requestName", requestName);

        Intent mIntent = new Intent(AdminServiceMenu.this, AdminServeStatusActivity.class);
        mIntent.putExtra("SERVICE_SELECTED", requestName);
        startActivity(mIntent);

    }

    private void getAllCountForAllServices() {

        for (Button mButton : btnArray) {
            ServiceCountMonitorHelper monitorHelper = new ServiceCountMonitorHelper();
            monitorHelper.getCount(mButton, database);
        }
    }
}
