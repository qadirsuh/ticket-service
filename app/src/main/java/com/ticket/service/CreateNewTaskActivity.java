package com.ticket.service;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CreateNewTaskActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLetters, btnNewStudentReg, btnModuleReg, btnNotifications, btnTimeTable, btnFinanceCashier, btnCertificates, btnResults;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_task);

        mProgressDialog = Utils.progressDialog("Loading...", this);

        btnLetters = findViewById(R.id.btnLetters);
        btnLetters.setOnClickListener(this);
        btnNewStudentReg = findViewById(R.id.btnNewStudentReg);
        btnNewStudentReg.setOnClickListener(this);
        btnModuleReg = findViewById(R.id.btnModuleReg);
        btnModuleReg.setOnClickListener(this);
        btnNotifications = findViewById(R.id.btnNotifications);
        btnNotifications.setOnClickListener(this);
        btnTimeTable = findViewById(R.id.btnTimeTable);
        btnTimeTable.setOnClickListener(this);
        btnFinanceCashier = findViewById(R.id.btnFinanceCashier);
        btnFinanceCashier.setOnClickListener(this);
        btnCertificates = findViewById(R.id.btnCertificates);
        btnCertificates.setOnClickListener(this);
        btnResults = findViewById(R.id.btnResults);
        btnResults.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        mProgressDialog.show();
        String requestName = v.getTag().toString().replace(" ", "_");
        Log.e("requestName", requestName);
        final String deviceId = Utils.getUserId(CreateNewTaskActivity.this);

        Map<String, Object> newTaskData = new HashMap<>();
        newTaskData.put("UserId", deviceId);
        newTaskData.put("isCompleted", false);
        newTaskData.put("createdAt", System.currentTimeMillis());
        newTaskData.put("service", requestName);

        saveNewTicket(requestName, newTaskData);
    }

    private void saveNewTicket(final String selectedService, final Map<String, Object> newTaskData) {

        final String dateToday = Utils.getTodayDate();

        final DatabaseReference servicePath = database.getReference("tickets/" + dateToday + "/" + selectedService + "");
        // Query uidRef = servicePath.orderByChild("isCompleted").equalTo(false);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mProgressDialog.dismiss();
                try {
                    // Log.e("onQueueChanged", dataSnapshot.getValue().toString());
                    Log.e("dataSnapshot count", dataSnapshot.getChildrenCount() + "");
                    long token_count_issued = dataSnapshot.getChildrenCount();
                    String userNewTicketNumber = "TSM-" + (token_count_issued + 1);

                    DatabaseReference ref = database.getReference("tickets/" + dateToday + "/" + selectedService + "");
                    newTaskData.put("ticketNumber", userNewTicketNumber);
                    DatabaseReference newTaskId = servicePath.push();
                    newTaskId.setValue(newTaskData);

                    String ticketKey = newTaskId.getKey();

                    // Save the Key in SharedPref too
                    Log.e("ticketKey", ticketKey + "");
                    Utils.saveTicketKey(CreateNewTaskActivity.this, ticketKey);
                    Utils.saveService(CreateNewTaskActivity.this, selectedService);
                    Utils.saveTicketNumber(CreateNewTaskActivity.this, userNewTicketNumber);
                    startActivity(new Intent(CreateNewTaskActivity.this, TicketStatusActivity.class));
                    Toast.makeText(CreateNewTaskActivity.this, "Ticket Created.", Toast.LENGTH_SHORT).show();
                    finish();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        servicePath.addListenerForSingleValueEvent(valueEventListener);
    }
}
