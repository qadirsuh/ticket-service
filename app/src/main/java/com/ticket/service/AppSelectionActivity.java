package com.ticket.service;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AppSelectionActivity extends AppCompatActivity {

    private Button btnClientApp, btnAdminApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_selection);

        btnClientApp = findViewById(R.id.btnClientApp);
        btnAdminApp = findViewById(R.id.btnAdminApp);

        btnClientApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.saveAppType(AppSelectionActivity.this, "CLIENT_APP");
                startActivity(new Intent(AppSelectionActivity.this, MainActivity.class));
                finish();
            }
        });

        btnAdminApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.saveAppType(AppSelectionActivity.this, "ADMIN_APP");
                startActivity(new Intent(AppSelectionActivity.this, AdminActivity.class));
                finish();
            }
        });
    }
}
