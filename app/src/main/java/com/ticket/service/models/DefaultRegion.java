package com.ticket.service.models;

public class DefaultRegion {

    public String address;
    public double lat;
    public double lng;
    public int radius;

    public DefaultRegion(){

    }

    public DefaultRegion(String address, double lat, double lng, int radius) {
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.radius = radius;
    }
}
