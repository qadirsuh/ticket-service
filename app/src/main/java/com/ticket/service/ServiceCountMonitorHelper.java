package com.ticket.service;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ServiceCountMonitorHelper {

    public void getCount(final Button btn, FirebaseDatabase database) {

        final String dateToday = Utils.getTodayDate();
        String service = btn.getTag().toString();
        final String buttonTextOriginal = btn.getText().toString().split(" \\(")[0];
        final DatabaseReference servicePath = database.getReference("tickets/" + dateToday + "/" + service + "");
        Query uidRef = servicePath.orderByChild("isCompleted").equalTo(false);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    // Log.e("onQueueChanged", dataSnapshot.getValue().toString());
                    Log.e("count queue" + btn.getText(), dataSnapshot.getChildrenCount() + "");
                    long unProcessedTicketsCount = dataSnapshot.getChildrenCount();
                    btn.setText(buttonTextOriginal + " (" + unProcessedTicketsCount + ")");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }

}
