/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ticket.service;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.provider.Settings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

class Utils {

    public static final String PREFS_NAME = "PREF";
    public static final String KEY_TASK_FIREBASE_KEY = "TASK_FIREBASE_KEY";
    public static final String KEY_TASK_SERVICE = "TASK_SERVICE";
    public static final String KEY_APP_TYPE = "APP_TYPE";
    public static final String KEY_TICKET_NUMBER = "TICKET_NUMBER";
    public static final String KEY_ADMIN_USER_NAME = "ADMIN_USERNAME";
    public static final String KEY_ADMIN_NAME = "ADMIN_NAME";

    static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_location_updates";

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     *
     * @param requestingLocationUpdates The location updates state.
     */
    static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    /**
     * Returns the {@code location} object as a human readable string.
     *
     * @param location The {@link Location}.
     */
    static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated,
                DateFormat.getDateTimeInstance().format(new Date()));
    }

    static String getTodayDate() {
        Date cDate = new Date();
        String fDate = new SimpleDateFormat("dd-MM-yyyy").format(cDate);
        return fDate;
    }

    static String getUserId(Context mContext) {
        String deviceId = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    static void saveTicketKey(Context mContext, String firebaseKey) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_TASK_FIREBASE_KEY, firebaseKey);
        editor.apply();
    }

    static void saveService(Context mContext, String service) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_TASK_SERVICE, service);
        editor.apply();
    }

    static void saveTicketNumber(Context mContext, String ticketNumber) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_TICKET_NUMBER, ticketNumber);
        editor.apply();
    }

    static String getKeyTicketNumber(Context mContext) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String key = sharedPref.getString(KEY_TICKET_NUMBER, null);
        return key;
    }

    static String getTicketKey(Context mContext) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String key = sharedPref.getString(KEY_TASK_FIREBASE_KEY, null);
        return key;
    }

    static String getService(Context mContext) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String key = sharedPref.getString(KEY_TASK_SERVICE, null);
        return key;
    }

    static String getAppType(Context mContext) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String key = sharedPref.getString(KEY_APP_TYPE, "null");
        return key;
    }

    static void saveAppType(Context mContext, String appType) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_APP_TYPE, appType);
        editor.apply();
    }

    static void saveAdminUserName(Context mContext, String userName) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_ADMIN_USER_NAME, userName);
        editor.apply();
    }

    static String getAdminUserName(Context mContext) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String key = sharedPref.getString(KEY_ADMIN_USER_NAME, null);
        return key;
    }

    static void saveAdminName(Context mContext, String userName) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_ADMIN_NAME, userName);
        editor.apply();
    }

    static String getAdminName(Context mContext) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String key = sharedPref.getString(KEY_ADMIN_NAME, null);
        return key;
    }

    public static ProgressDialog progressDialog(String title, Context mContext) {

        try {

            ProgressDialog dialog = new ProgressDialog(mContext); // this = YourActivity
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage(title);
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            //dialog.show();

            return dialog;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void showInfoAlert(Context mContext, String title, String message, String button_yes, String button_no, final Runnable runnablePositive, final Runnable runnableNegative) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon(R.drawable.ic_alert_success);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(button_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if (runnablePositive != null) {
                    runnablePositive.run();
                }
            }
        });

        alertDialog.setNegativeButton(button_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (runnableNegative != null) {
                    runnableNegative.run();
                }
            }
        });

        alertDialog.show();
    }

}
