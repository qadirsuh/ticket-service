package com.ticket.service;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class AdminServeStatusActivity extends AppCompatActivity {

    private TextView txtCurrentServing, txtRemainingUsers;
    private Button btnDone;

    private Intent mIntent;
    private String selectedService;

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_serve_status);

        txtCurrentServing = findViewById(R.id.txtCurrentServing);
        txtRemainingUsers = findViewById(R.id.txtRemainingUsers);
        btnDone = findViewById(R.id.btnDone);

        mIntent = getIntent();
        mProgressDialog = Utils.progressDialog("Loading...", this);

        selectedService = mIntent.getStringExtra("SERVICE_SELECTED");

        getAllTasksInQueue();
        getCurrentServingTicket();
    }


    private void getAllTasksInQueue() {

        mProgressDialog.show();
        final String dateToday = Utils.getTodayDate();

        final DatabaseReference servicePath = database.getReference("tickets/" + dateToday + "/" + selectedService + "");
        Query uidRef = servicePath.orderByChild("isCompleted").equalTo(false);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    // Log.e("onQueueChanged", dataSnapshot.getValue().toString());
                    Log.e("dataSnapshot count", dataSnapshot.getChildrenCount() + "");
                    long unProcessedTicketsCount = dataSnapshot.getChildrenCount();
                    txtRemainingUsers.setVisibility(View.VISIBLE);
                    if (unProcessedTicketsCount > 0) {
                        txtRemainingUsers.setText("Users still in queue: " + (unProcessedTicketsCount - 1));
                    } else {
                        txtRemainingUsers.setText("Users still in queue: 0");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }

    private void getCurrentServingTicket() {

        String dateToday = Utils.getTodayDate();
        DatabaseReference ref = database.getReference("tickets/" + dateToday + "/" + selectedService + "");
        Query uidRef = ref.orderByChild("isCompleted").equalTo(false).limitToFirst(1);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProgressDialog.dismiss();

                if (dataSnapshot != null) {
                    try {

                        if (dataSnapshot.getChildrenCount() > 0) {

                            for (final DataSnapshot data : dataSnapshot.getChildren()) {
                                String ticketNumberCurrentlyServing = data.child("ticketNumber").getValue().toString();
                                txtCurrentServing.setText("Currently Serving\nTicket Number\n----\n" + ticketNumberCurrentlyServing + "\n---");

                                btnDone.setVisibility(View.VISIBLE);
                                txtRemainingUsers.setVisibility(View.VISIBLE);
                                btnDone.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mProgressDialog.show();
                                        markCurrentServingAsCompleted(data.getKey());
                                    }
                                });

                                break;
                            }
                        } else {

                            btnDone.setVisibility(View.GONE);
                            txtRemainingUsers.setVisibility(View.GONE);
                            if (txtRemainingUsers.getText().toString().equalsIgnoreCase("Users still in queue: 0")) {
                                txtCurrentServing.setText("Queue is Empty");
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }

    private void markCurrentServingAsCompleted(String taskId) {

        String dateToday = Utils.getTodayDate();
        DatabaseReference ref = database.getReference("tickets/" + dateToday + "/" + selectedService + "/" + taskId);
        Log.e("ref", ref + "");
        Map<String, Object> updateData = new HashMap<>();
        updateData.put("isCompleted", true);
        updateData.put("completedBy", Utils.getAdminUserName(AdminServeStatusActivity.this));
        updateData.put("completedAt", System.currentTimeMillis());
        ref.updateChildren(updateData);
    }

}
